function() {

  // initialize variables
  var env = karate.env; // get system property 'karate.env'
  var protocol = 'https';


  karate.log('karate.env system property was:', env);

  // karate configurations
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  karate.configure('ssl',true);


  var config={};
  config.baseSiteId = karate.properties['basesiteId'];
  var port = 9006;
  var basePath = '/ufscommercewebservices/v2/'
  var baseAuthPath = '/authorizationserver/oauth/'



  if (!env || env=='awsqa') {
    env = 'awsqa';
    config.env= env;
    config.clientId='android';
    config.clientSecret='android123';
    config.scope='extended';
    config.baseUrl = protocol + '://52.208.230.100:' + port + basePath;
    config.baseAuthUrl = protocol + '://52.208.230.100:' + port + baseAuthPath;
  }
  else if(env=='awsstg'){
    port = 9002;
    config.env= env;
    config.clientId='android';
    config.clientSecret='android123';
    config.scope='extended';
    config.baseUrl = protocol + '://52.31.80.240:' + port +  basePath;
    config.baseAuthUrl = protocol + '://52.31.80.240:' + port + baseAuthPath;
  }
  else if(env=='qa'){
    config.env= env;
    config.clientId='android';
    config.clientSecret='android123';
    config.scope='extended';
    config.baseUrl = protocol + '://qa-apihbs.ufs.com' +  basePath;
    config.baseAuthUrl = protocol + '://qa-apihbs.ufs.com' + baseAuthPath;
  }
  else if(env=='stage'){
    config.env= env;
    config.clientId='android';
    config.clientSecret='android123';
    config.scope='basic';
    config.baseUrl = protocol + '://stage-apihbs.ufs.com' +  basePath;
    config.baseAuthUrl = protocol + '://stage-apihbs.ufs.com'  + baseAuthPath;
  }
  else if (env == 'prod') {
    config.env= env;
    config.clientId='android';
    config.clientSecret='android123';
    config.scope='extended';
    config.baseUrl = protocol + '://apihbs.ufs.com' +  basePath;
    config.baseAuthUrl = protocol + '://apihbs.ufs.com'  + baseAuthPath;
    config.prodConfirmURL=karate.properties['prodConfirmURL'];
    config.prodNewsletterConfirmUrl=karate.properties['prodNewsletterConfirmUrl'];
  }else{
    env = 'awsqa';
    config.env= env;
    config.clientId='android';
    config.clientSecret='android123';
    config.scope='extended';
    config.baseUrl = protocol + '://52.208.230.100:' + port + basePath;
    config.baseAuthUrl = protocol + '://52.208.230.100:' + port + baseAuthPath;
  }

  var result = karate.callSingle('classpath:OAuth/oauth2.feature', config);
  config.authInfo = {authTime: result.time, authToken: result.token, tokenType: 'Bearer '};

  karate.log('Environment is:', env);
  karate.log('Oauth Token is:', config.authInfo.authToken);
  karate.log('Expires in:', config.authInfo.authTime);
  karate.log('Token Type is :', config.authInfo.tokenType);

  return config;
}