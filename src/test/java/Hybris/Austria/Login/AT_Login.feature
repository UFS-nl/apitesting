@AT
Feature: Feature to test business types

  Background:
    * def loginJSON = read('classpath:resources/AT/AT_Login.json')

    * def logIn = call read('classpath:Hybris/Generic/Login/Login.feature') loginJSON
    * def accessToken = logIn.accessToken
    * def refreshToken = logIn.refreshToken
    * def sifuToken = logIn.sifuToken

    * print refreshToken
    * print accessToken


  @login
  Scenario: Login accessToken and refreshToken not null
    * match accessToken != null
    * match refreshToken != null
    * match sifuToken != null
