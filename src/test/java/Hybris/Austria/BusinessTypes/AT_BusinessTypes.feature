@AT
Feature: Feature to test business types for Austria

  @businessTypes
  Scenario: Test the businessTypes for AT
    * def businessTypes = call read('classpath:Hybris/Generic/BusinessTypes/BusinessTypes.feature')
    And match businessTypes.businessTypesArray != null
    And match businessTypes.businessTypesArray.businessList contains read('classpath:resources/AT/AT_BusinessTypes.json')
