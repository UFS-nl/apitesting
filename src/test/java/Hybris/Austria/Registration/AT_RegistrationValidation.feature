@AT
Feature: Feature to test the validations during user registrations.

  Background:
    * def regData = read('classpath:resources/AT/AT_RegistrationValidation.json')

  @validations
  Scenario: AT User Validation -  For invalid combinations
    * def result = call read('classpath:Hybris/Generic/Registration/RegistrationValidations.feature') regData
    * match $result[*].responseStatus == [409,400,400]




