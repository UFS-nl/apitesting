@AT
Feature: Feature to test api for registerting new customers

  Background:
    * url baseUrl

    * def tokenTypeVal = authInfo.tokenType
    * def tokenVal = authInfo.authToken
    * def registrationJSON = read('classpath:resources/AT/AT_ValidUserRegistration.json')

    * def randomNumberFn = function() {  return Math.floor(100000 + Math.random() * 900000);}
    * def randomTextFn = function(){ return Math.random().toString(36).substring(7);}
    * def mailId = randomTextFn()+ 'AT'+ randomNumberFn() + '@mailsac.com'
    * print 'mailId used for creation-- ', mailId
    * set registrationJSON.uid = mailId
    * print 'Before Registration --', registrationJSON



    * configure headers = read('classpath:header.js')
    * print 'Basesite Id -' , baseSiteId
    * eval if (env == 'prod') registrationJSON.confirmUrl = prodConfirmURL
    * eval if (env == 'prod') registrationJSON.newsletterConfirmUrl = prodNewsletterConfirmUrl
    * print registrationJSON

  @newuser
  Scenario: Valid New User Registration
    Given path  baseSiteId + '/users'
    And request registrationJSON
    When method post
    Then status 201
    And print response



