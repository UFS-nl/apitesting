@DE
Feature: Feature to test business types for Germany

  @businessTypes
  Scenario: Test the businessTypes for DE
    * def businessTypes = call read('classpath:Hybris/Generic/BusinessTypes/BusinessTypes.feature')
    And match businessTypes.businessTypesArray != null
    And match businessTypes.businessTypesArray == read('classpath:resources/DE/DE_BusinessTypes.json')

