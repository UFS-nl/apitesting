Feature: Feature to login in to website. To be passed userid, password, country code, site code

  Background:
    * url baseUrl
    * def tokenTypeVal = authInfo.tokenType
    * def tokenVal = authInfo.authToken

    * configure headers = read('classpath:header.js')

  Scenario:
    Given path  baseSiteId + '/login'
    And request { userId: '#(userId)', password: '#(password)' , countryCode: '#(countryCode)' , siteCode: '#(siteCode)'}
    When method post
    Then status 200
    And match response.type == 'userLoginResponseWsDTO'

    * print response
    * def accessToken = response.access_token
    * def refreshToken = response.refresh_token
    * def sifuToken = response.sifuToken