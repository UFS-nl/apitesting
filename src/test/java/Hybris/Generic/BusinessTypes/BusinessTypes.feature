Feature: Feature to test business types

  Background:
    * url baseUrl

    * def tokenTypeVal = authInfo.tokenType
    * def tokenVal = authInfo.authToken


    * configure headers = read('classpath:header.js')
    * print 'Basesite Id -' , baseSiteId


  Scenario: Test the businessTypes
    Given  path  baseSiteId + '/businessTypes'
    And  param fields = 'BASIC'
    When method get
    Then status 200
    * print response
    * def businessTypesArray = response
