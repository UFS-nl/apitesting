Feature: Feature to test the validations during user registrations.

  Background:
    * url baseUrl
    * def tokenTypeVal = authInfo.tokenType
    * def tokenVal = authInfo.authToken

    * configure headers = read('classpath:header.js')
    * print 'Basesite Id -' , baseSiteId


  Scenario: Registration of user
    Given path  baseSiteId + '/users'
    And request __arg
    When method post
    * def validatedRes = responseStatus
    * def result = response
    * print responseStatus



