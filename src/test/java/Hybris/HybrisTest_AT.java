package Hybris;

import com.intuit.karate.KarateOptions;
import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;

@KarateOptions(tags = {"@AT","~@ignore"})
public class HybrisTest_AT {
    @Test
    public void testParallel() {

        try {
            FileUtils.deleteDirectory(new File("target/AT"));
            FileUtils.deleteDirectory(new File("target/cucumber-html-reports"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String karateOutputPath = "target/AT/cucumber-html-reports/";
        KarateStats stats = CucumberRunner.parallel(getClass(), 5, karateOutputPath);
        generateReport(karateOutputPath);
        System.out.println("karateOutputPath---"+karateOutputPath);
        assertTrue("there are scenario failures", stats.getFailCount() == 0);
    }

    public static void generateReport(String karateOutputPath) {
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
        List<String> jsonPaths = new ArrayList(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
        Configuration config = new Configuration(new File("target"), "ufscommerceservices");
        config.addClassifications("Environment", System.getProperty("karate.env"));
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("basesiteId", "ufs_at");
        System.setProperty("prodConfirmURL", "http://www.unileverfoodsolutions.at/profil-erstellen/registration-completion.html");
        System.setProperty("prodNewsletterConfirmUrl", "http://www.unileverfoodsolutions.at/newsletter-abonnieren/newsletter-abonnieren-completion.html");
    }
}
