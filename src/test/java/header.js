function fn() {
  var token = tokenVal;
  var tokenType = tokenTypeVal;
  karate.log("Token in header -",token);
  var contentType = 'application/json'
  if (token) {
    return {
        Authorization: tokenType + token,
        Accept: 'application/json'
        //request_id: uuid + '' // convert the java uuid into a string
    };
  } else {
    return {};
  }
}